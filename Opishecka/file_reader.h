#ifndef FILE_READER_H
#define FILE_READER_H

#include "Exchange_banks.h"

void read(const char* file_name, exchange_banks* array[], int& size);

#endif