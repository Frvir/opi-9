#include "filter.h"
#include <cstring>
#include <iostream>

exchange_banks** filter(exchange_banks* array[], int size, bool (*check)(exchange_banks* element), int& result_size) {
	exchange_banks** result = new exchange_banks * [size];
	result_size = 0;
	for (int i = 0; i < size; i++)
	{
		if (check(array[i]))
		{
			result[result_size++] = array[i];
		}
	}
	return result;
}

bool check_exchange_banks_by_name(exchange_banks* element)
{
	return strcmp(element->name.name, "�����������") == 0;
}

bool check_exchange_banks_by_sales(exchange_banks* element)
{
	return element->change.sell <2.5;
}
