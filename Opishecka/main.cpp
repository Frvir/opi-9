#include <iostream>

using namespace std;

#include "Exchange_banks.h"
#include "file_reader.h"
#include "constants.h"
#include "filter.h"

void output(exchange_banks* exchanges) {
    // ����� �������� �����
    cout << "����........: ";
    cout << exchanges/*[i]*/->name.name << " ";
    //����� ����� ������
    cout << "����...........: ";
    cout << exchanges/*[i]*/->change.buy << " ";
    cout << exchanges/*[i]*/->change.sell << ". ";
    cout << '\n';
    // ����� ������
    cout << "�����.....: ";
    cout << '"' << exchanges/*[i]*/->adress << '"';
    cout << '\n';
    cout << '\n';
}




int main()
{
    setlocale(LC_ALL, "Russian");
    cout << "������������ ������ �8. GIT\n";
    cout << "������� �4. ����� ������\n";
    cout << "�����: ����� ������\n";
    cout << "������: 14\n\n";
    exchange_banks* exchanges[MAX_FILE_ROWS_COUNT];
    int size;
    try
    {
        read("data.txt", exchanges, size);
        cout << "***** ����� ������ *****\n\n";
        for (int i = 0; i < size; i++) {
            output(exchanges[i]);
        }

        bool (*check_function)(exchange_banks*) = NULL; // check_function -    ,    bool,
                                                           //        book_subscription*
        cout << "\n     :\n";
        cout << "1)������� ����� ������ �� ���� ���������� ����� ������������ (� ��������)\n";
        cout << "2)������� ����� ������ � ������ ��������� ������, � ������� ������� ������ 2,5\n";
        cout << "\n   : ";
        int item;
        cin >> item;
        cout << '\n';
        switch (item)
        {
        case 1:
            check_function = check_exchange_banks_by_name; //       
            cout << "***** ��������� ����� ������������ *****\n\n";
            break;
        case 2:
            check_function = check_exchange_banks_by_sales; //       
            cout << "***** ������� ������ 2,5 *****\n\n";
            break;


        default:
            throw "  ";
        }
        if (check_function)
        {
            int new_size;
            exchange_banks** filtered = filter(exchanges, size, check_function, new_size);
            for (int i = 0; i < new_size; i++)
            {
                output(filtered[i]);
            }
            delete[] filtered;
        }
        for (int i = 0; i < size; i++)
        {
            delete exchanges[i];
        }

    }
    catch (const char* error)
    {
        cout << error << '\n';
    }
    return 0;
}
