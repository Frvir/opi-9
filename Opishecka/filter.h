#ifndef FILTER_H
#define FILTER_H

#include "Exchange_banks.h"

exchange_banks** filter(exchange_banks* array[], int size, bool (*check)(exchange_banks* element), int& result_size);
bool check_exchange_banks_by_name(exchange_banks* element);

bool check_exchange_banks_by_sales(exchange_banks* element);


#endif